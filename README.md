# schnaps.sh
Raspberry Pi timelapse script that handles low light captures

[View demo](demo.mp4?raw=true)

Uses `sunwait` to get daylight status.  
https://sourceforge.net/projects/sunwait4windows/

Despite the URL suggesting otherwise, it compiles fine on Linux and ARM.

Other solutions I've found uses API calls to Yahoo and other silly places, to get sunrise times.
The movement and rotation of planets and stars is a math problem, and can be done much faster and
reliably locally. Also, don't ever rely on Yahoo for anything vaguely important.
