#!/bin/bash
#schnaps.sh by Jens Rex <jens@jensrex.dk>
#https://github.com/JensRex/schnaps.sh
#
#Invokes raspistill with nighttime parameters at the appropriate time.
#Because automatic mode is terrible for low light scenes.
#
#Images are piped to avoid using local storage. Uploads to remote
#location, because storing potentially years of data on a Pi is a
#terrible idea.
#
#jpegtran is used to losslessly recompress images. Saves about 10%
#which starts to matter when you're generating thousands of images.

#WebDAV target to upload pictures to.
username="CHANGEME"
password="CHANGEME"
address="https://www.example.com/timelapse/"

#Where's the camera? Coordinates only valid for planet Earth, sorry.
location="55.33N 11.44E"

status=$(sunwait poll civil $location)
if [ $status = "NIGHT" ]; then
	drc="high" #Dynamic Range Compression
	expmode="verylong" #Exposure mode
	shspeed="3000000" #Shutter speed (microseconds)
elif [ $status = "DAY" ]; then
	drc="off"
	expmode="auto"
	shspeed="0" #Auto
else
	echo "sunwait error. Check location parameter."
	exit 1
fi

raspistill \
	--width 1440 \
	--height 1080 \
	--output - \
	--thumb none \
	--nopreview \
	--annotate 4 \
	--annotate "%F %R" \
	--rotation 180 \
	--shutter $shspeed \
	--exposure $expmode \
	--drc $drc \
	|\
jpegtran \
	-progressive \
	-optimize \
	-copy all \
	|\
curl \
	--digest \
	--user $username:$password \
	--upload-file - $address$(date +%Y%m%d%H%M%S).jpg \
	--output /dev/null \
	--silent \
	--cert-status
